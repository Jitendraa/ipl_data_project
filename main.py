"""
This module tells about the plotting using matplot library.
The extraction function of the data from csv file is done here.

functions:
    extract_matches
    extract_deliveries
    main
"""
import csv
import sys
from os import path
sys.path.append(
    path.join(path.dirname(__file__), "ipl_analytics/csv_analytics/")
)
from matches_played_per_year import compute_and_plot_matches_played_per_year
from matches_won import compute_and_plot_matches_won_by_team_per_year
from extra_runs import compute_and_plot_every_team_extra_runs_2016
from economical_bowlers_2015 import compute_and_plot_top_economical_bowlers
from top_batsman_2017 import compute_and_plot_top_batsman_runs


ROOT_PATH = path.dirname(__file__)
DATA_ROOT_PATH = path.join(ROOT_PATH, 'data_set')


def extract_matches(matches_file):
    """
    The function extracts data from a csv file into a list of dictionary.

    Parameters:
        matches_file (csv file):  All matche details of IPL in a csv file.

    returns:
        list of dictionary: raw data into the format of dictionary.
    """
    matches = []    # matches is a list of dictionary.
    with open(DATA_ROOT_PATH + "/" + matches_file) as file:
        file_reader = csv.DictReader(file)
        for row in file_reader:
            matches.append(dict(row))

    return matches


def extract_deliveries(deliveries_csv):
    """
    The function extracts data from a csv file into a list of dictionary.

    Parameters:
        deliveries_csv (csv file): Every match details of IPL in a csv file.

    returns:
        list of dictionary: Every match details.
    """
    deliveries = []
    with open(DATA_ROOT_PATH + "/" + deliveries_csv) as file:
        file_reader = csv.DictReader(file)
        for row in file_reader:
            deliveries.append(row)

    return deliveries


def main():  # pragma: no cover
    """
    This function just call the modules and plot the graph accordingly.

    returns : 0
    """
    matches = extract_matches('matches.csv')
    deliveries = extract_deliveries('deliveries.csv')
    # 1.Plot the number of matches played per year of all the years in IPL.
    compute_and_plot_matches_played_per_year(matches)
    # 2. Plot a stacked bar chart of matches won of all teams over all the
    #  years of IPL.
    compute_and_plot_matches_won_by_team_per_year(matches)
    # 3. For the year 2016 plot the extra runs conceded per team.
    compute_and_plot_every_team_extra_runs_2016(matches, deliveries)
    # 4. For the year 2015 plot the top economical bowlers.
    compute_and_plot_top_economical_bowlers(matches, deliveries)
    # 5. Top batsman of 2017
    compute_and_plot_top_batsman_runs(matches, deliveries)


if __name__ == "__main__":  # pragma: no cover
    main()

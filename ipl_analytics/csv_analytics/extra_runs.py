"""
This module is about extra runs given by every team
IPl season 2016.

functions:
    every_team_extra_runs_2016
    plot_every_team_extra_runs_2016
    compute_and_plot_every_team_extra_runs_2016
"""
import matplotlib.pyplot as plt
from helper import match_id_range_ina_season


def every_team_extra_runs_2016(matches, deliveries):
    """
    Compute the extra runs given by every team in 2016.

    parameters:
        matches (list of dictionary): Every match details of the IPL.
        deliveries (list of dictionary): Every match's deliveries of the IPL.

    returns:
        every_team_extra_runs (dict): every team extra runs of 2016.
    """

    every_team_extra_runs = {}
    match_id_range = match_id_range_ina_season(matches, 2016)
    for delivery in deliveries:
        if match_id_range is not None and int(delivery['match_id']) in \
             match_id_range:
            if delivery['bowling_team'] in every_team_extra_runs.keys():
                every_team_extra_runs[delivery['bowling_team']] += \
                 int(delivery['extra_runs'])
            else:
                every_team_extra_runs[delivery['bowling_team']] = \
                 int(delivery['extra_runs'])

    print(every_team_extra_runs)
    return every_team_extra_runs


def plot_every_team_extra_runs_2016(every_team_extra_runs):  # pragma: no cover
    """
    It plots a graph about how much extra runs are given by every team in 2016.

    parameters:
        every_team_extra_runs (dict): every team extra runs of 2016.

    returns: 0
    """
    teams = list(every_team_extra_runs.keys())
    extra_runs = list(every_team_extra_runs.values())
    plt.bar(teams, extra_runs, width=0.35)
    plt.xlabel("IPL Teams")
    plt.ylabel("Extra Runs")
    plt.title("EXTRA RUNS BY EVERY IPL-TEAM IN 2016")
    plt.show()


def compute_and_plot_every_team_extra_runs_2016(matches, deliveries):  # pragma: no cover
    """
    This function just calls both functions 'plot_every_team_extra_runs_2016'
    and 'plot_every_team_extra_runs_2016'.

    parameters:
        matches (list of dictionary): Every match details of the IPL.
        deliveries (list of dictionary): Every match deliveries details of IPL.

    returns: 0
    """
    every_team_extra_runs = every_team_extra_runs_2016(matches, deliveries)
    plot_every_team_extra_runs_2016(every_team_extra_runs)

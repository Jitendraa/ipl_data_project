"""
Computaion of number of matches won by a team in all the
year and plot a stack bar graph in this data.

functions:
    every_team_year_win
    plot_matches_won_by_all_teams
    add_two_list
    compute_and_plot_matches_won_by_team_per_year
"""

import matplotlib.pyplot as plt
from helper import sort_dict_by_keys


def every_team_year_win(matches):
    """
    It computes number of matches won by every team.

    parameters:
        matches (list of dictionary): Every match raw details of the IPL.

    returns:
        matches_won(dict): Every year team wins in the IPL.
    """
    matches_won = {}
    for match in matches:
        if match['winner'] == '':   # pragma: no cover
            continue
        if match['winner'] == 'Rising Pune Supergiant':
            match['winner'] = 'Rising Pune Supergiants'

        if match['winner'] in matches_won:
            if match['season'] in matches_won[match['winner']]:
                matches_won[match['winner']][match['season']] += 1
            else:
                matches_won[match['winner']][match['season']] = 1
        else:
            matches_won[match['winner']] = dict()
            matches_won[match['winner']][match['season']] = 1

    for dic in matches_won.values():
        for year in range(2008, 2018):
            new_year = str(year)
            if new_year not in list(dic.keys()):
                dic[str(new_year)] = 0
    print(matches_won)
    return matches_won


def plot_matches_won_by_all_teams(every_team_year_win):  # pragma: no cover
    """
    This function plots a stack bar graph. And the graph represents
    number of matches won by every team in a year.

    parameters:
        every_team_year_win (dict): Every year team wins in the IPL.

    returns: 0
    """
    years_win = []
    print(every_team_year_win)
    teams = []
    for team, dic in every_team_year_win.items():
        temp_wins = 0
        teams.append(team)
        every_team_year_win[team], temp_wins = sort_dict_by_keys(dic)
        years_win.append(temp_wins)

    years = [year for year in range(2008, 2018)]
    previous_team_wins = [0] * len(years)
    for wins in range(len(years_win)):
        plt.bar(years, years_win[wins], width=0.35, bottom=previous_team_wins)
        previous_team_wins = add_two_list(years_win[wins], previous_team_wins)

    plt.xlabel("No of matches")
    plt.ylabel("Years")
    plt.title("NUMBER OF MATCHES WON BY EVERY TEAM")
    plt.legend(teams, loc='upper left')
    plt.show()


def add_two_list(arr1, arr2):  # pragma: no cover
    """
    It adds two array with their respective positions.

    parameters:
        arr1 (list): List contains integers.
        arr2 (list): List contains integers.

    returns:
        total_sum (list): The sum of two list with thier respective positions.
    """
    total_sum = []
    for i in range(len(arr1)):
        temp = arr1[i] + arr2[i]
        total_sum.append(temp)

    return total_sum


def compute_and_plot_matches_won_by_team_per_year(matches):  # pragma: no cover
    """
    computation and plotting of graph of matches won by each team in
    every year.

    parameters:
        matches (list of dictionary): Every match details of the IPL.

    returns: 0
    """
    matches_won_by_team_per_year = every_team_year_win(matches)
    plot_matches_won_by_all_teams(matches_won_by_team_per_year)

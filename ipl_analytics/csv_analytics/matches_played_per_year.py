"""
This module computes and plots number of matches played per year.

functions:
    matches_played_per_year
    plot_matches_played_per_year
    compute_and_plot_matches_played_per_year
"""
import matplotlib.pyplot as plt
from helper import sort_dict_by_keys


def matches_played_per_year(matches):
    """
    It gives a sorted dictionary according to no of matches
    played every in every season of file.

    parameter:
        matches (list  of dictionary): Every match details.

    returns:
        sorted dictioary (dict): Total matches played per year
    """
    matches_per_year = {}
    for match in matches:
        if int(match['season']) in matches_per_year:
            matches_per_year[int(match['season'])] += 1
        else:
            matches_per_year[int(match['season'])] = 1

    matches_per_year, temp = sort_dict_by_keys(matches_per_year)
    # print(matches_per_year)
    return matches_per_year


def plot_matches_played_per_year(matches_per_year):  # pragma: no cover
    """
    It plots a bar graph. Where x-axis => Years
                                y-axis => number of matches played

    parameters:
        sorted dictionary (matches per year)
    returns 0
    """
    years = matches_per_year.keys()
    no_of_matches = matches_per_year.values()
    print(years)
    print(no_of_matches)
    plt.bar(years, no_of_matches, width=0.35)
    plt.xlabel("Year")
    plt.ylabel("Number of matches")
    plt.show()


def compute_and_plot_matches_played_per_year(matches):  # pragma: no cover
    """
    This function computes number of matches played per year
    and plot a bar graph.

    parameters:
        sorted dictioary (dict): Total matches played per year

    retunns: 0
    """
    matches_per_year = matches_played_per_year(matches)
    plot_matches_played_per_year(matches_per_year)

'''
This modules gives top 10 batsman of 2017.

functions:
    all_batsman_runs_2017
    plot_top_batsman_runs
    compute_and_plot_top_batsman_runs
'''

import matplotlib.pyplot as plt
from helper import match_id_range_ina_season


def all_batsman_runs_2017(matches, deliveries):
    """
    This function gives you exact total_runs of all the batsman
    in IPL 2017.

    parameters:
        matches (list of dictionary): Every match details of the IPL.
        deliveries (list of dictionary): Every match deliveries details of IPL.

    retruns:
        all_batsman_runs (dict): all batsman runs of 2017
    """
    all_batsman_runs = {}
    match_id_range = match_id_range_ina_season(matches, 2017)

    for row in deliveries:
        if match_id_range is not None \
             and int(row['match_id']) in match_id_range:
            if row['batsman'] in all_batsman_runs:
                all_batsman_runs[row['batsman']] += int(row['batsman_runs'])
            else:
                all_batsman_runs[row['batsman']] = int(row['batsman_runs'])

    return all_batsman_runs


def plot_top_batsman_runs(all_batsman_runs):  # pragma: no cover
    """
    It gives you top 10 batsman of 2017 and also plots a bar graph.

    parameters:
        all_batsman_runs(dict): all batsman runs of 2017

    returns: 0
    """
    all_batsman_runs_sorted = sorted(all_batsman_runs.items(),
                                     key=lambda kv: (kv[1], kv[0]))
    batsman_names = []
    total_runs = []

    for row in range(len(all_batsman_runs_sorted)-10,
                     len(all_batsman_runs_sorted)):
        batsman_names.append(all_batsman_runs_sorted[row][0])
        total_runs.append(all_batsman_runs_sorted[row][1])

    plt.bar(batsman_names, total_runs, width=0.35)
    plt.xlabel("Batsman Names")
    plt.ylabel("Runs")
    plt.title("Top 10 BATSMAN OF 2017")
    plt.show()


def compute_and_plot_top_batsman_runs(matches, deliveries):  # pragma: no cover
    """
    This function combines the functionaly of total runs of all
    batsman and plots the bar graph.

    parameters:
        matches (list of dictionary): Every match details of the IPL.
        deliveries (list of dictionary): Every match deliveries details of IPL.

    returns: 0
    """
    all_batsman_runs = all_batsman_runs_2017(matches, deliveries)
    plot_top_batsman_runs(all_batsman_runs)

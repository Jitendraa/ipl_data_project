"""
It has some important functions which is used in many modules.

functions:
    sort_dict_by_keys
    match_id_range_ina_season
"""


def sort_dict_by_keys(dic):
    """
    Sort the dictionary on the basis of key values.

    parameters: 
        dic (dictionary): some raw data in dictionary.

    returns:
        dictionary: a sorted dictionary.
    """
    resultant_dict = {}
    keys = sorted(dic.keys())
    for key in keys:
        resultant_dict[int(key)] = dic[key]

    return resultant_dict, list(resultant_dict.values())


def match_id_range_ina_season(matches, season):
    """
    Finding a match_id range of a gieven specific season.

    parameters:
        matches (dictionary): every match details in dictionary.
        season (int): a specific year

    returns:
        range: None or valid match_id range
    """
    if not matches:
        return range(0, 1)
    id_range = []
    season_flag = False
    for match in matches:
        if match['season'] == str(season):
            id_range.append(match['id'])
            season_flag = True

    if season_flag:
        return range(int(id_range[0]), int(id_range[-1])+1)
    else:
        return None

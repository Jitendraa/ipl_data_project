"""
This module calculates economy of all the bowler of IPL season.

Functions:
    economical_bowlers
    plot_top_economical_bowlers
    compute_and_plot_top_economical_bowlers
"""
import matplotlib.pyplot as plt
from helper import match_id_range_ina_season


def economical_bowlers(matches, deliveries):
    """
    This function computes the economy of all the bowler.

    parameters:
        matches (list of dictionary): Every match details of the IPL.
        deliveries (list of dictionary): Every match's deliveries of the IPL.

    returns:
        a dictionary (All bowler economies)
    """
    bowlers_details = {}
    bowlers_economy = {}
    match_id_range = match_id_range_ina_season(matches, 2015)

    for delivery in deliveries:
        if match_id_range is not None \
             and int(delivery['match_id']) in match_id_range:

            if delivery['bowler'] in bowlers_details:
                bowlers_details[delivery['bowler']][0] += (
                    int(delivery['total_runs'])
                    - int(delivery['bye_runs'])
                    - int(delivery['legbye_runs'])
                )
                bowlers_details[delivery['bowler']][1] += 1
            else:
                bowlers_details[delivery['bowler']] = [0] * 2
                bowlers_details[delivery['bowler']][0] = (
                    int(delivery['total_runs'])
                    - int(delivery['bye_runs'])
                    - int(delivery['legbye_runs'])
                )
                bowlers_details[delivery['bowler']][1] = 1

    for name, details in bowlers_details.items():
        overs = details[1] / 6
        if overs >= 1:
            bowlers_economy[name] = details[0] / overs

    return bowlers_economy


def plot_top_economical_bowlers(bowlers_economy):  # pragma: no cover
    """
    This function plots only top 10 bowler with best economy.

    parameters:
        bowlers_economy (dictionary): all bowlers economy.

    returns: 0
    """
    bowler_and_economy = sorted(
        bowlers_economy.items(), key=lambda kv: (kv[1], kv[0])
    )
    bowlers = []
    economies = []
    count = 1
    for bowler, economy in bowler_and_economy:
        if count <= 10:
            bowlers.append(bowler)
            economies.append(economy)
            count += 1
        else:
            break
    print(bowlers)
    print(economies)
    plt.plot(bowlers, economies)
    plt.bar(bowlers, economies, width=0.35)
    plt.xlabel("Bowlers")
    plt.ylabel("Economy")
    plt.title("BEST BOWLERS OF 2015")
    plt.show()


def compute_and_plot_top_economical_bowlers(matches, deliveries):  # pragma: no cover
    """
    This function computes result and plots the graph.

    parameters:
        matches (list of dictionary): Every match details of the IPL.
        deliveries (list of dictionary): Every match deliveries details of IPL.

    returns: 0
    """
    bowlers_economy = economical_bowlers(matches, deliveries)
    plot_top_economical_bowlers(bowlers_economy)

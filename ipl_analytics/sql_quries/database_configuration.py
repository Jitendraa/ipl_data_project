"""
The module is basically used for connecting from the database on
the basis of result of query.

functions:
    connect_database
    execute_query
"""
from configparser import ConfigParser
import psycopg2
from encrypt_decrypt import decryption


parser = ConfigParser()
parser.read('config.ini')


def connect_database():
    """
    This function is used for connecting the database.

    returns:
        cur (connection object): A connection object for referencing database.
    """
    connection = psycopg2.connect(
        database=parser.get('database', 'database_db'),
        user=parser.get('database', 'user'),
        password=decryption(),
        host=parser.get('database', 'host'),
        port=parser.get('database', 'port')
    )
    cur = connection.cursor()

    return cur


def execute_query(cursor, query):
    """
    This function excutes the SQL query.

    parmameters:
        cursor(connection object): A database referencing object.
        query: A sql query to be executed.
    returns:
    respone(query result): The result of the qeury after fetching from the database.
    """
    cursor.execute(query)
    response = cursor.fetchall()

    return response

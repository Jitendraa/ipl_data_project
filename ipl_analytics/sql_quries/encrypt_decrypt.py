"""
This module is about the encryting the KEY.

functions:
    encryption
    decryption
"""
from configparser import ConfigParser
from cryptography.fernet import Fernet
KEY = b'GCOJfgLJv0uFMBQkpL7nGBdr4bFnAVI1H6HFE65K5AI='


def encryption(password='*********'):  # pragma: no cover
    """
    This function returns an encrpted password with the help of key.

    parameters:
        password (string): String can contains numbers, letters etc. anything.

    returns:
        encrypted: an ecypted password which almost unreadble.
    """
    encode = password.encode()
    f_key = Fernet(KEY)
    encrypt_b = f_key.encrypt(encode)
    encrypted = encrypt_b.decode()

    return encrypted


PARSER = ConfigParser()
PARSER.read('config.ini')


def decryption():
    """
    This function gives a decryped password

    returns:
        decrypted: a decrypted password
    """
    f_key = Fernet(KEY)
    decrypted = f_key.decrypt(PARSER.get('database', 'password').encode()).decode()

    return decrypted

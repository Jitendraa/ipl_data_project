"""
This module solves the IPL dataset problems using SQL quries.

functions:
    matches_played_per_year_query
    matches_won_of_all_team_query
    extra_runs_2016_query
    economical_bowlers_2015_query
    top_batsman_runs_2017_query
"""
from database_configuration import connect_database, execute_query


def matches_played_per_year_query(cursor, matches_table):
    """
    This function returns number of matches played per year in all season
    of IPL using sql quries.

    parameters:
        cursor (connection object): Database refrencing object.
        matches_table (table): A data set table of matches.csv

    returns:
        matches_year (list of tuples): Number of matches played in every year.
    """
    matches_year = execute_query(
        cursor, """ SELECT season, COUNT(season) as no_of_matches
                    FROM {}
                    GROUP BY season
                    ORDER BY season;
                """.format(matches_table)
    )
    return matches_year


def matches_won_of_all_team_query(cursor, matches_table):
    """
    This function returns number of matches won by a team
    every season.

    parameters:
        cursor (connection object): Database refrencing object.
        matches_table (table): A data set table of matches.csv

    returns:
        matches_won_teams (list of tuples): number of matches won by a team
                                            every season.
    """
    matches_won_teams = execute_query(
        cursor, """SELECT season,
                          CASE WHEN winner like 'Rising Pune Supergiant'
                          THEN 'Rising Pune Supergiants'
                          else winner end as winner,
                          count(winner)
                    FROM {}
                    WHERE winner is not NULL
                    GROUP BY winner, season
                    ORDER BY winner, season;
                """.format(matches_table)
    )

    return matches_won_teams


def extra_runs_2016_query(cursor, matches_table, deliveries_table):
    """
    This function computes the extra runs conceded by every team
    in IPL season 2016.

    parameters:
        cursor (connection object): Database refrencing object.
        matches_table (table): A data set table of matches.csv
        deliveries (table): A data set table of deliveries.csv

    returns:
        every_team_extra_runs (list of tuples): The extra runs conceded by
                                                every team in IPL season 2016.
    """
    every_team_extra_runs = execute_query(
        cursor, """SELECT bowling_team,
                          SUM(extra_runs) as extra_runs
                          FROM {}, {}
                          WHERE id=match_id and season=2016
                          GROUP BY bowling_team
                          ORDER BY extra_runs DESC;
                """.format(matches_table, deliveries_table)
    )

    return every_team_extra_runs


def economical_bowlers_2015_query(cursor, matches_table, deliveries_table):
    """
    It ruturns top 10 bowler of IPL season 2015 with
    their economy.

    parameters:
        cursor (connection object): Database refrencing object.
        matches_table (table): A data set table of matches.csv
        deliveries (table): A data set table of deliveries.csv

    returns:
        baller_economy (list of tuples): top 10 bowler of IPL season 2015 with
                                         their economy.

    """
    baller_economy = execute_query(
        cursor, """
                WITH bowler_details as
                    (
                        SELECT bowler,
                           SUM(total_runs)-SUM(legbye_runs)-SUM(bye_runs)
                           as runs,
                           SUM(
                               case when ball in (1,2,3,4,5,6) then 1
                               else 0 end
                           ) as balls
                        FROM {}, {}
                        where id=match_id and season=2015
                        GROUP BY bowler
                    )
                SELECT bowler, runs/(balls/6.0) as economy
                FROM bowler_details
                ORDER BY economy
                LIMIT 10;
                """.format(matches_table, deliveries_table)
    )
    return baller_economy


def top_batsman_runs_2017_query(cursor, matches_table, deliveries_table):
    """
    This function is about computing the top 10 bastsman
    of IPL season 2017.

    parameters:
        cursor (connection object): Database refrencing object.
        matches_table (table): A data set table of matches.csv
        deliveries (table): A data set table of deliveries.csv

    returns:
        top_batsman (list of tuples): the top 10 bastsman
                                      of IPL season 2017.
    """
    top_batsman = execute_query(
        cursor, """SELECT batsman, SUM(batsman_runs) as total_runs
                    FROM {}, {}
                    WHERE id=match_id and season=2017
                    GROUP BY batsman
                    ORDER BY total_runs DESC
                    LIMIT 10;
                """.format(matches_table, deliveries_table)
    )

    return top_batsman


def main():  # pragma: no cover
    """
    Excuting all the queries one after another.

    returns 0
    """
    cursor = connect_database()
    matches = """matches"""
    deliveries = """deliveries"""
    # Question 1- number of matches played per year of all the years in IPL.
    result = matches_played_per_year_query(cursor, matches)
    print(result, '\n')
    # # Question 2 - matches won of all teams over all the years of IPL.
    result = matches_won_of_all_team_query(cursor, matches)
    print(result, '\n')
    # Question 3 - For the year 2016 plot the extra runs conceded per team.
    result = extra_runs_2016_query(cursor, matches, deliveries)
    print(result, '\n')
    # Question 4 - For the year 2015 plot the top economical bowlers.
    result = economical_bowlers_2015_query(cursor, matches, deliveries)
    print(result, '\n')
    # Question 5 - Top 10 batsman of 2017.
    result = top_batsman_runs_2017_query(cursor, matches, deliveries)
    print(result, '\n')


if __name__ == "__main__":  # pragma: no cover
    main()

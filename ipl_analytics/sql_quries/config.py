"""
This module is about database configuration. And it
has all the deatails related to database.

sections: database
"""
from configparser import ConfigParser
from encrypt_decrypt import encryption


config = ConfigParser()

config['database'] = {
    'database_db': 'ipl_database',
    'user': 'postgres',
    'password': encryption(),
    'host': '127.0.0.1',
    'port': '5432'
}

with open('./config.ini', 'w') as file:
    config.write(file)

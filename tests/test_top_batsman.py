"""
This module is basically for testing. At first it checks computation
of result through a csv file and even result is tested from the
database of ipl_database.

functions:
    test_empty_csv_file
    test_all_batsman_runs_2017
    test_top_batsman_runs_2017_query
"""
import sys
from os import path
sys.path.append(
    path.join(path.dirname(__file__), "../ipl_analytics/csv_analytics/")
)
sys.path.append(
    path.join(path.dirname(__file__), "../ipl_analytics/sql_quries/")
)


from main import extract_deliveries, extract_matches
from top_batsman_2017 import all_batsman_runs_2017
from sql_quries import top_batsman_runs_2017_query
from database_configuration import connect_database


def test_empty_csv_file():
    """
    Testing the functionality of all_batsman_runs_2017 function.
    In this testing function, the function is being checke by
    an empty function.
    """
    matches = extract_matches('empty_matches.csv')
    deliveries = extract_deliveries('empty_deliveries.csv')
    result = all_batsman_runs_2017(matches, deliveries)
    print(result)
    expected_result = {}
    assert result == expected_result, "test failed"


def test_all_batsman_runs_2017():
    """
    This testing function is checking the functionality of all_batsman_runs
    through a valid csv file.
    """
    matches = extract_matches('dummy_extra_runs.csv')
    deliveries = extract_matches("dummy_top_batsman.csv")
    result = all_batsman_runs_2017(matches, deliveries)
    # print(result)
    expected_result = {
        "David Warner": 8,
        "Virat Kohli": 16,
        "MS Dhoni": 24,
        "Sachin": 16,
        "Maxwell": 8
    }
    assert result == expected_result, "Test failed all batsman tuns."


# Testing  of SQL query.
def test_top_batsman_runs_2017_query():
    """
    This testing function is testing the functionality of
    top_batsman_runs_2017_query. For testig dataset is being
    taken from the database of ipl_database.
    """
    cursor = connect_database()
    dummy_extra_runs = """dummy_extra_runs"""
    dummy_top_batsman = """dummy_top_batsman"""
    result = top_batsman_runs_2017_query(
        cursor, dummy_extra_runs, dummy_top_batsman
    )
    expected_result = [
        ('MS Dhoni', 24),
        ('Virat Kohli', 16),
        ('Sachin', 16),
        ('David Warner', 8),
        ('Maxwell', 8)
    ]
    assert result == expected_result, 'test failed top batsman.'

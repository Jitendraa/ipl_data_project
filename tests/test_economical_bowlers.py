"""
This module for testing for economical_bowlers of 2015.

functions:
    test_empty_csv
    test_through_csv
    test_through_csv1
    test_economical_bowlers_2015_query
"""
import sys
from os import path
sys.path.append(
    path.join(path.dirname(__file__), "../ipl_analytics/csv_analytics/")
)
sys.path.append(
    path.join(path.dirname(__file__), "../ipl_analytics/sql_quries/")
)

from main import extract_deliveries, extract_matches
from economical_bowlers_2015 import economical_bowlers
from sql_quries import economical_bowlers_2015_query
from database_configuration import connect_database


def test_empty_csv():
    """
    This functionality of economical_bowler is being tested through
    an empty csv file.

    returns: 0
    """
    matches = extract_matches('empty_matches.csv')
    deliveries = extract_deliveries('empty_deliveries.csv')
    result = economical_bowlers(matches, deliveries)
    expected_result = {}
    assert result == expected_result, "test failed economy of ballers."


def test_through_csv():
    """
    Did not give the exact season and testing according to that.
    In this case match_id_range is None.

    returns: 0
    """
    matches = extract_matches('test_matches.csv')
    deliveries = extract_deliveries('test_deliveries.csv')
    result = economical_bowlers(matches, deliveries)
    expected_result = {}
    assert result == expected_result, "test failed economy of ballers."


def test_through_csv1():
    """
    Testing through the exact season 2015 and with a valid match_id_range.

    returns: 0
    """
    matches = extract_matches('dummy_extra_runs.csv')
    deliveries = extract_deliveries('dummy_economy.csv')
    result = economical_bowlers(matches, deliveries)
    expected_result = {'Shami': 2.0, 'Bumrah': 8.0, 'BrettLee': 18.0}
    assert result == expected_result, "Test failed economy of ballers."


# Testing of SQL query
def test_economical_bowlers_2015_query():
    """
    Two dummy table are created in ipl_datbase for testing to
    find economy of bowler as well as it can give you top 10 with
    best economy baller of IPL season 2015.

    returns: 0
    """
    dummy_extra_runs = """dummy_extra_runs"""
    dummy_economy = """dummy_economy"""
    cursor = connect_database()
    result = economical_bowlers_2015_query(
        cursor, dummy_extra_runs, dummy_economy
    )
    expected_result = [
        ('Shami', 2.0),
        ('Bumrah', 8.0),
        ('BrettLee', 18.0)
    ]
    assert result == expected_result, 'test failed'

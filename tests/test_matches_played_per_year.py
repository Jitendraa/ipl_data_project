"""
This module is basically for testing. In this module matches_played_per
year function are being tested through both csv and database respectively.

functions:
    test_csv_file
    test_empty_csv_file
    test_matches_per_year1
    test_matches_playes_year2
    test_matches_per_year_query
"""
import sys
from os import path
sys.path.append(
    path.join(path.dirname(__file__), "../ipl_analytics/csv_analytics/")
)
sys.path.append(
    path.join(path.dirname(__file__), "../ipl_analytics/sql_quries/")
)

from matches_played_per_year import matches_played_per_year
from main import extract_matches
from sql_quries import matches_played_per_year_query
from database_configuration import connect_database


def test_matches_per_year1():
    """
    Testing the fuction matches_played per year explicitly
    without taking any csv_file.
    """
    mock_matches = [
        {
            'match_id': 1,
            'season': 2008
        },
        {
            'match_id': 2,
            'season': 2008
        },
        {
            'match_id': 3,
            'season': 2009
        }
    ]
    result = matches_played_per_year(mock_matches)
    assert result == {2008: 2, 2009: 1}, "test failed"


def test_csv_file():
    """
    Testing a dummy csv file containing of few rows.
    """
    matches = extract_matches('dummy_matches1.csv')
    result = matches_played_per_year(matches)
    # print(result)
    assert result == {2008: 4, 2017: 3}, "test failed."
    # result = matches_played_per_year('')


def test_matches_playes_year2():
    """
    Testing the functionality of matches_played_per_year
    directly.
    """
    mock_matches = [
        {'match_id': 5, "season": 2018},
        {'match_id': 8, "season": 2012},
        {'match_id': 34, "season": 2018},
        {'match_id': 56, "season": 2019}
    ]
    result = matches_played_per_year(mock_matches)
    assert result == {2018: 2, 2012: 1, 2019: 1}, "test failed 2019"


def test_empty_csv_file():
    """
    Testing the functionality of matches_played_year through an empty csv file.
    """
    matches = extract_matches('empty_matches.csv')
    result = matches_played_per_year(matches)
    expectecd_result = {}
    assert result == expectecd_result, "test failed."


# Testing of SQL quries
def test_matches_per_year_query():
    """
    The functionality of matches_played_per_year_query is being tested
    for this dataset is taken form the ipl_database.
    """
    cursor = connect_database()
    dummy_matches1 = """dummy_matches1"""
    result = matches_played_per_year_query(cursor, dummy_matches1)
    expectecd_result = [(2008, 4), (2017, 3)]
    assert result == expectecd_result, "test failed"

"""
This module checking the funtionality of the matches_won module.

functions:
    test_every_team_year_win1
    test_every_team_year_win2
    test_through_csv_file
    test_matches_won_of_all_team_query
"""

import sys
from os import path
sys.path.append(
    path.join(path.dirname(__file__), "../ipl_analytics/csv_analytics/")
)
sys.path.append(
    path.join(path.dirname(__file__), "../ipl_analytics/sql_quries/")
)


from main import extract_matches
from matches_won import every_team_year_win
from sql_quries import matches_won_of_all_team_query
from database_configuration import connect_database


def test_every_team_year_win1():
    """
    An empty file is given to check the boundry conditions.
    """
    matches = extract_matches('empty_matches.csv')
    result = every_team_year_win(matches)
    expected_result = {}
    assert result == expected_result, "test failed"


def test_every_team_year_win2():
    """
    Testing the function 'every_team_year_win' explicitly.
    """
    mock_matches = [
        {
            'match_id': 1,
            'season': '2008',
            'winner': 'Rising Pune Supergiants'
        },
        {
            'match_id': 2,
            'season': '2009',
            'winner': "CSK"
        },
        {
            'match_id': 3,
            'season': '2009',
            'winner': "CSK"
        }
    ]

    result = every_team_year_win(mock_matches)
    expected_result = {
        "CSK": {
            '2009': 2,
            '2008': 0,
            '2010': 0,
            '2011': 0,
            '2012': 0,
            '2013': 0,
            '2014': 0,
            '2015': 0,
            '2016': 0,
            '2017': 0
        },
        "Rising Pune Supergiants": {
            '2008': 1,
            '2009': 0,
            '2010': 0,
            '2011': 0,
            '2012': 0,
            '2013': 0,
            '2014': 0,
            '2015': 0,
            '2016': 0,
            '2017': 0
        }
    }
    assert result == expected_result, "test failed"


def test_through_csv_file():
    """
    Testing a small dataset of a csv file and monitering the
    result accordinly.
    """
    user_input = extract_matches('dummy_matches_won.csv')
    result = every_team_year_win(user_input)
    expected_result = {
        "CSK": {
            "2008": 2,
            "2009": 1,
            "2010": 2,
            "2011": 1,
            "2012": 0,
            "2013": 0,
            "2014": 0,
            "2015": 0,
            "2016": 0,
            "2017": 0
        },
        "KKR": {
            "2008": 2,
            "2011": 1,
            "2009": 0,
            "2010": 0,
            "2012": 0,
            "2013": 0,
            "2014": 0,
            "2015": 0,
            "2016": 0,
            "2017": 0
        },
        "RCB": {
            "2008": 1,
            "2009": 1,
            "2010": 0,
            "2011": 0,
            "2012": 0,
            "2013": 0,
            "2014": 0,
            "2015": 0,
            "2016": 0,
            "2017": 0
        },
        "Rising Pune Supergiants": {
            "2012": 1,
            "2008": 0,
            "2009": 0,
            "2010": 0,
            "2011": 0,
            "2013": 0,
            "2014": 0,
            "2015": 0,
            "2016": 1,
            "2017": 0
        }
    }
    assert result == expected_result, "test failed"


def test_matches_won_of_all_team_query():
    """
    Testing the functionality of matches_won_of_all_team_query with help of
    ipl_database.
    """
    cursor = connect_database()
    dummy_matches_won = """dummy_matches_won"""
    result = matches_won_of_all_team_query(cursor, dummy_matches_won)
    expected_result = [
        (2008, 'CSK', 2),
        (2009, 'CSK', 1),
        (2010, 'CSK', 2),
        (2011, 'CSK', 1),
        (2008, 'KKR', 2),
        (2011, 'KKR', 1),
        (2008, 'RCB', 1),
        (2009, 'RCB', 1),
        (2012, 'Rising Pune Supergiants', 1),
        (2016, 'Rising Pune Supergiants', 1)
    ]
    assert result == expected_result, "test failed"

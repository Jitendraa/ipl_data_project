"""
This module is for testing extra_runs functionality through
csv and ipl_database both.

functions:
    test_empty_file
    test_every_team_extra_runs1
    test_every_team_extra_runs2
    test_every_team_extra_runs3
    test_extra_runs_2016_query
"""
import sys
from os import path
sys.path.append(
    path.join(path.dirname(__file__), "../ipl_analytics/csv_analytics/")
)
sys.path.append(
    path.join(path.dirname(__file__), "../ipl_analytics/sql_quries/")
)


from extra_runs import every_team_extra_runs_2016 
from main import extract_deliveries, extract_matches
from sql_quries import extra_runs_2016_query
from database_configuration import connect_database

def test_empty_file():
    """
    Testing an empty csv file with only attributes.
    """
    matches = extract_matches('empty_matches.csv')
    deliveries = extract_deliveries("empty_deliveries.csv")
    result = every_team_extra_runs_2016(matches, deliveries)
    expected_result = {}
    assert result == expected_result, "test failed"


def test_every_team_extra_runs1():
    """
    Testing the functionality of every_team_extra_runs function using two
    small instances of mathces.csv and deliveries.csv.
    """
    matches = extract_matches('dummy_extra_runs.csv')
    deliveries = extract_deliveries('dummy_extra_runs_deliveries.csv')
    result = every_team_extra_runs_2016(matches, deliveries)
    expected_result = {
        "CSK": 64,
        "KKR": 52,
        "RCB": 98,
        "SH": 30,
        "DD": 27
    }
    assert result == expected_result, "test failed"


def test_every_team_extra_runs2():
    """
    Testing the functionality of every_team_extra_runs function
    explitcitly.
    """
    matches = [
        {
            'id': '1',
            'season': '2015'
        }
    ]
    deliveries = [
        {
            'match_id': 1,
            'bowling_team': 'CSC',
            'extra_runs': '15'
        }
    ]
    result = every_team_extra_runs_2016(matches, deliveries)
    expected_result = {}
    assert result == expected_result, 'test failed'


def test_every_team_extra_runs3():
    """
    """
    matches = [
        {
            'id': '1',
            'season': '2015',
        },
        {
            'id': '2',
            'season': '2016',
        }

    ]
    deliveries = [
        {
            'match_id': '2',
            'bowling_team': 'CSK',
            'extra_runs': '15'
        },
        {
            'match_id': '2',
            'bowling_team': 'KKR',
            'extra_runs': '14'
        }
    ]
    expected_result = {
        'CSK': 15,
        'KKR': 14
    }
    result = every_team_extra_runs_2016(matches, deliveries)
    assert result == expected_result, "test failed"


# Testing of SQL query
def test_extra_runs_2016_query():
    """
    Testing extra_runs_2016_query functionality with the help
    of ipl_database.
    """

    cursor = connect_database()
    dummy_extra_runs = """dummy_extra_runs"""
    dummy_extra_runs_deliveries = """dummy_extra_runs_deliveries"""
    result = extra_runs_2016_query(
        cursor, dummy_extra_runs, dummy_extra_runs_deliveries
    )
    expected_result = [
        ('RCB', 98),
        ('CSK', 64),
        ('KKR', 52),
        ('SH', 30),
        ('DD', 27)
    ]
    assert result == expected_result, 'test failed'

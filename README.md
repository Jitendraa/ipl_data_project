# IPL Data Project
### Description

This project is about an analysis of IPL  seasons on the basis of records from 2008 to 2017. The project has
mainly three parts. These three are followings:
                    Part 1 - Computation of result and Plotting of graphs
                    Part 2 - Unit Testing
                    Part 3- SQL(Postgres)

#### Part 1: 
    Code python functions that will transform the raw csv data into a data structure in a format suitable for plotting with matplotlib.

    Generate the following plots ...

    1. Plot the number of matches played per year of all the years in IPL.
    2. Plot a stacked bar chart of matches won of all teams over all the years of IPL.
    3. For the year 2016 plot the extra runs conceded per team.
    4. For the year 2015 plot the top economical bowlers.
    5. Discuss a "Story" you want to tell with the given data. As with part 1, prepare the data structure
       and plot with matplotlib.
       For this section I calculted top 10 batsman of IPL season 2017.

#### Part 2:
    Rewrite the data project. Create your own smaller dataset - like 5 matches and 15 deliveries. Manually set the result for the unit tests.

#### Part 3:
    Import your test source into a Postgresql database i.e. ipl_test_db. Write SQL queries for each of the above questions. Save it into a text file. Use the psql prompt or pgadmin to write queries.

    If you want to use psql, become familiar with \l, \c, \dt, \d+, \dd commands.

    1. Now, connect to the database from Python using the psycopg2 library
    2. Define separate functions which execute each query.
    3. These functions must return the same data as your Python business logic functions, so that the corresponding plot 4. functions can be shared between them.
    5. Compare the SQL output with your original Python functions' output.

    ## Environments:
    
        This project was developed in the follwing environment:
            Python 3.7.3 [GCC 7.3.0] :: Anaconda, Inc. on linux
            Postgresql-11

The directory tree structure is following:

        .
        ├── data_set
        │   ├── deliveries.csv
        │   ├── dummy_deliv_economy.csv
        │   ├── dummy_deliveries.csv
        │   ├── dummy_economy.csv
        │   ├── dummy_extra_runs.csv
        │   ├── dummy_extra_runs_deliveries.csv
        │   ├── dummy_matches1.csv
        │   ├── dummy_matches_won.csv
        │   ├── dummy_top_batsman.csv
        │   ├── empty_deliveries.csv
        │   ├── empty_matches.csv
        │   ├── matches.csv
        │   ├── test_deliveries.csv
        │   └── test_matches.csv
        ├── ipl_analytics
        │   ├── csv_analytics
        │   │   ├── economical_bowlers_2015.py
        │   │   ├── extra_runs.py
        │   │   ├── helper.py
        │   │   ├── matches_played_per_year.py
        │   │   ├── matches_won.py
        │   │   └── top_batsman_2017.py
        │   └── sql_quries
        │       ├── config.ini
        │       ├── config.py
        │       ├── database_configuration.py
        │       ├── encrypting.py
        │       ├── generate_key.py
        │       ├── key.key
        │       ├── sql_quries.py
        │       ├── sql.txt
        │       └── test_sql_quries.py
        ├── main.py
        ├── README.md
        ├── requirements.txt
        └── tests
            ├── test_economical_bowlers.py
            ├── test_extra_runs.py
            ├── test_matches_played_per_year.py
            ├── test_matches_won.py
            └── test_top_batsman.py

---------------------------------------------------------------------------------------------------------------------------

## CSV files description:
​
    -matches.csv : It contains the records of each match of IPL in the years between 2008 to 2017.
    -deliveries.csv : It contains full details of each delivery of each match of IPL in the years
                      between 2008 to 2017. Like batsman details, bowler details etc.


    Files for testing are following:

        dummy_deliv_economy.csv
        dummy_deliveries.csv
        dummy_economy.csv
        dummy_extra_runs.csv
        dummy_extra_runs_deliveries.csv
        dummy_matches1.csv
        dummy_matches_won.csv
        dummy_top_batsman.csv
        empty_deliveries.csv
        empty_matches.csv
        matches.csv
        test_deliveries.csv
        test_matches.csv
---------------------------------------------------------------------------------------------------------------------------

### Excution of Projects

### How to excute part 1:
   --> command : python main.py
   After excution of this command we would be able see those five plots which is mentioned above.
---------------------------------------------------------------------------------------------------------------------------
### How to excute part 2:
    -- The whole testing is done with the help of pytest.
    --> command:  "pytest" to excute all testing modules.
    But make sure cure current directory should be like "~/tests/"
    Note:
        For every question, testing of both plotting and SQL queries are done in the same module.
        And all five testing modules are present in the same directory. 
 --------------------------------------------------------------------------------------------------------------------------
 ### How to excute part 3:
    --> Command: "python sql_quries.py"
    And this module is present in "~/ipl_analytics/sql_quries/sql_quries.py"

    ## NOTE 1:

    Be confrim about the key.
    key : GCOJfgLJv0uFMBQkpL7nGBdr4bFnAVI1H6HFE65K5AI=

    ## NOTE 2:
    To get the key "generate_key.py" must be excuted and saved into a file. And make sure you excute this file just once.
    The key is stored into key.key file which is used for encryption and decryption as well.

### Data set:
    It is better to take the data set from https://www.kaggle.com/manasgarg/ipl.
    During extraction of the csv files make sure, you set the path according to your system or else you can even add a
    configuraton file for this thing.